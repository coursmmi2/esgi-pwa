'use strict';

import page from '/node_modules/page/page.mjs';

(async () => {
  const app = document.querySelector('#app main');

  const result = await fetch('/data/spacex.json');
  const cloneResult = result.clone();
  const data = await result.json();

  caches.open('runtime-cache').then(cache => {
    cache.put('/data/spacex.json', cloneResult);
  })

  const skeleton = app.querySelector('.skeleton');
  skeleton.setAttribute('hidden', '');

  const homeCtn = app.querySelector('[page=home]');
  const readCtn = app.querySelector('[page=read]');

  const pages = [
    homeCtn,
    readCtn
  ];

  page('/', async (ctx) => {
    const module = await import('./view/home.js');
    const Home = module.default;

    Home(homeCtn, data);

    pages.forEach(page => page.removeAttribute('active'));
    homeCtn.setAttribute('active', true);
    const docTitle = document.head.querySelector('title');
    document.title = `${docTitle.dataset.base} - Home`;
  });

  page('/read/:slug', async (ctx) => {
    const module = await import('./view/read.js');
    const Read = module.default;

    const readStyle = document.head.querySelectorAll('#read');
    if (readStyle.length === 0) {
      const style = document.createElement('link');
      style.id = 'read';
      style.rel= 'stylesheet';
      style.href = '/style/read.css';
      document.head.appendChild(style);
    }

    const slug = ctx.params.slug;

    const article = data.find(item => _slugify(item.content.title) === slug);
    Read(readCtn, article);

    pages.forEach(page => page.removeAttribute('active'));
    readCtn.setAttribute('active', true);

    const docTitle = document.head.querySelector('title');
    document.title = `${docTitle.dataset.base} - ${article.content.title}`;

    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
  });

  page();
})();

function _slugify(text) {
  return text.toString().toLowerCase()
    .replace(/\s+/g, '-')           // Replace spaces with -
    .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
    .replace(/\-\-+/g, '-')         // Replace multiple - with single -
    .replace(/^-+/, '')             // Trim - from start of text
    .replace(/-+$/, '');            // Trim - from end of text
}
